/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#ifndef ERIS_SHEET_VIEW_PROPERTIES_HPP
#define ERIS_SHEET_VIEW_PROPERTIES_HPP

#include "eris/visible_range.hpp"

#include <ixion/address.hpp>

namespace orcus { namespace spreadsheet {

class sheet;

}}

namespace eris {

class ViewModel;
class DocModel;

class SheetViewProperties
{
    ViewModel& m_view_model;
    ixion::abs_address_t m_view_pos;
    VisibleRange m_visible_range;

    double m_visible_width; /// in twips
    double m_visible_height; /// in twips

    bool m_visible_row_range_valid:1;
    bool m_visible_col_range_valid:1;

    void refresh_visible_range();

public:
    SheetViewProperties(ViewModel& viewModel, sheet_t sheet_pos);
    ~SheetViewProperties();

    void invalidate_visible_range();
    void set_view_position(ixion::row_t row, ixion::col_t col);
    const ixion::abs_address_t& get_view_position() const;

    void update_visible_row_range(int height);
    void update_visible_col_range(int width);

    const VisibleRange& get_visible_range() const;

    void move_right();
    void move_left();
    void move_up();
    void move_down();

    void page_up();
    void page_down();

    void move_home();
    void move_row_home();
};

}

#endif
