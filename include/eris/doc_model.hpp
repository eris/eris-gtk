/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#ifndef ERIS_DOC_MODEL_HPP
#define ERIS_DOC_MODEL_HPP

#include "types.hpp"

#include <orcus/spreadsheet/document.hpp>

#include <string>

namespace eris {

class DocModel
{
public:
    DocModel();
    ~DocModel();

    col_width_t get_default_col_width() const;
    row_height_t get_default_row_height() const;

    col_t get_default_col_size() const;
    row_t get_default_row_size() const;

    void swap_document(orcus::spreadsheet::document& doc);

    const orcus::spreadsheet::document& get_doc() const;

private:
    col_width_t m_default_col_width;
    row_height_t m_default_row_height;
    col_t m_default_col_size;
    row_t m_default_row_size;
    orcus::spreadsheet::document m_document;
};

}

#endif
