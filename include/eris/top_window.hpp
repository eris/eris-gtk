/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#ifndef ERIS_TOP_WINDOW_HPP
#define ERIS_TOP_WINDOW_HPP

#include "eris/sheet_grid.hpp"
#include "eris/column_header.hpp"
#include "eris/row_header.hpp"

#include "eris/view_model.hpp"
#include "eris/doc_model.hpp"

#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/scrollbar.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/label.h>

namespace eris {

/**
 * The topmost window of Eris.
 */
class TopWindow : public Gtk::Window
{
public:
    TopWindow();
    ~TopWindow();

    ViewModel& get_view_model();
    const ViewModel& get_view_model() const;

    DocModel& get_doc_model();
    const DocModel& get_doc_model() const;

protected:
    void on_vbar_value_changed();
    void on_hbar_value_changed();

    virtual void on_size_allocate(Gtk::Allocation& allocation);
    virtual bool on_key_press_event(GdkEventKey* event);

private:
    void launch_file_chooser_dialog();
    void view_changed();

    bool on_load_timeout();

private:
    DocModel m_doc_model;
    ViewModel m_view_model;

    Glib::RefPtr<Gtk::Adjustment> m_vbar_adj;
    Glib::RefPtr<Gtk::Adjustment> m_hbar_adj;

    Gtk::Scrollbar m_vbar;
    Gtk::Scrollbar m_hbar;

    Gtk::Grid m_grid_content;
    Gtk::Grid m_grid_scrollbars;
    Gtk::VBox m_vbox_top;

    Gtk::HBox m_status_bar;
    Gtk::Label m_status_text;
    Gtk::ProgressBar m_load_progress;

    SheetGrid m_sheet_grid;
    ColumnHeader m_col_header;
    RowHeader m_row_header;
};

}

#endif
