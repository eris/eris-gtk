/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#ifndef ERIS_VIEW_MODEL_HPP
#define ERIS_VIEW_MODEL_HPP

#include "eris/view_properties.hpp"
#include "eris/visible_range.hpp"
#include "eris/sheet_view_properties.hpp"

#include <orcus/spreadsheet/types.hpp>

#include <string>
#include <vector>
#include <boost/ptr_container/ptr_vector.hpp>

namespace eris {

class DocModel;

class ViewModel
{
    const DocModel& m_doc_model;
    boost::ptr_vector<SheetViewProperties> m_sheet_views;
    orcus::spreadsheet::sheet_t m_cur_sheet;
    GlobalViewProperties m_global_view;

public:
    ViewModel(const DocModel& doc);

    const DocModel& get_doc_model() const;

    void init_sheet_views(size_t sheet_count);

    orcus::spreadsheet::sheet_t get_current_sheet_index() const;
    SheetViewProperties& get_current_sheet_view();
    const SheetViewProperties& get_current_sheet_view() const;

    const GlobalViewProperties& get_global_view_properties() const;

    std::string get_column_label(col_t col) const;
};

}

#endif
