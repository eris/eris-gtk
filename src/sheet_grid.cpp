/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/sheet_grid.hpp"
#include "eris/top_window.hpp"
#include "eris/view_model.hpp"
#include "eris/view_properties.hpp"

#include <iostream>
#include <sstream>

#include <gdkmm/general.h>
#include <gdkmm/screen.h>
#include <gtkmm/layout.h>

#include <orcus/spreadsheet/document.hpp>
#include <orcus/spreadsheet/sheet.hpp>
#include <ixion/model_context.hpp>
#include <ixion/formula_result.hpp>

using namespace std;
using namespace orcus::spreadsheet;

namespace eris {

SheetGrid::SheetGrid(TopWindow& tw) :
    Glib::ObjectBase("sheet-grid"),
    Gtk::Widget(),
    m_top_win(tw)
{
    set_has_window(true);
    set_hexpand(true);
    set_vexpand(true);

    //Install a style so that an aspect of this widget may be themed via a CSS
    //style sheet file:
    gtk_widget_class_install_style_property(
        GTK_WIDGET_CLASS(G_OBJECT_GET_CLASS(gobj())),
        g_param_spec_int("example_scale",
                         "Scale of Example Drawing",
                         "The scale to use when drawing. This is just a silly example.",
                         G_MININT, G_MAXINT, 100, G_PARAM_READABLE));

    m_style_provider = Gtk::CssProvider::create();
    Glib::RefPtr<Gtk::StyleContext> refStyleContext = get_style_context();
    refStyleContext->add_provider(m_style_provider,
                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

SheetGrid::~SheetGrid()
{
}

Gtk::SizeRequestMode SheetGrid::get_request_mode_vfunc() const
{
    return Gtk::Widget::get_request_mode_vfunc();
}

void SheetGrid::get_preferred_width_vfunc(int& minimum_width, int& natural_width) const
{
    minimum_width = 500;
    natural_width = 1000;
}

void SheetGrid::get_preferred_height_for_width_vfunc(
    int /* width */, int& minimum_height, int& natural_height) const
{
    minimum_height = 500;
    natural_height = 800;
}

void SheetGrid::get_preferred_height_vfunc(int& minimum_height, int& natural_height) const
{
    minimum_height = 500;
    natural_height = 800;
}

void SheetGrid::get_preferred_width_for_height_vfunc(
    int /* height */, int& minimum_width, int& natural_width) const
{
    minimum_width = 500;
    natural_width = 1000;
}

void SheetGrid::on_size_allocate(Gtk::Allocation& allocation)
{
    //Do something with the space that we have actually been given:
    //(We will not be given heights or widths less than we have requested, though
    //we might get more)

    //Use the offered allocation for this container:
    set_allocation(allocation);

    if (m_gdk_win)
    {
        m_gdk_win->move_resize(
            allocation.get_x(), allocation.get_y(),
            allocation.get_width(), allocation.get_height());
    }

    // Recalculate visible cell range.
    m_top_win.get_view_model().get_current_sheet_view().update_visible_col_range(allocation.get_width());
    m_top_win.get_view_model().get_current_sheet_view().update_visible_row_range(allocation.get_height());
}

void SheetGrid::on_map()
{
    //Call base class:
    Gtk::Widget::on_map();
}

void SheetGrid::on_unmap()
{
    //Call base class:
    Gtk::Widget::on_unmap();
}

void SheetGrid::on_realize()
{
    //Do not call base class Gtk::Widget::on_realize().
    //It's intended only for widgets that set_has_window(false).

    set_realized();

    //Get the themed style from the CSS file:
    get_style_property("example_scale", m_scale);

    if (!m_gdk_win)
    {
        //Create the GdkWindow:

        GdkWindowAttr attributes;
        memset(&attributes, 0, sizeof(attributes));

        Gtk::Allocation allocation = get_allocation();

        //Set initial position and size of the Gdk::Window:
        attributes.x = allocation.get_x();
        attributes.y = allocation.get_y();
        attributes.width = allocation.get_width();
        attributes.height = allocation.get_height();

        attributes.event_mask = get_events() | Gdk::EXPOSURE_MASK;
        attributes.window_type = GDK_WINDOW_CHILD;
        attributes.wclass = GDK_INPUT_OUTPUT;

        m_gdk_win = Gdk::Window::create(get_parent_window(), &attributes,
                                             GDK_WA_X | GDK_WA_Y);
        set_window(m_gdk_win);

        //set colors
        override_background_color(Gdk::RGBA("white"));
        override_color(Gdk::RGBA("rgb(215,215,215)"));

        //make the widget receive expose events
        m_gdk_win->set_user_data(gobj());
    }
}

void SheetGrid::on_unrealize()
{
    m_gdk_win.reset();

    //Call base class:
    Gtk::Widget::on_unrealize();
}

bool SheetGrid::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    double width = get_allocated_width();
    double height = get_allocated_height();

    // paint the background
    Gdk::Cairo::set_source_rgba(cr, get_style_context()->get_background_color());
    cr->paint();

    // draw the foreground
    Gdk::Cairo::set_source_rgba(cr, get_style_context()->get_color());
    cr->set_line_width(0.6);

    const ViewModel& viewModel = m_top_win.get_view_model();
    double dpi = viewModel.get_global_view_properties().get_dpi();
    Pango::FontDescription font = viewModel.get_global_view_properties().get_header_font();
    const VisibleRange& visRange = viewModel.get_current_sheet_view().get_visible_range();

    // Draw horizontal gridlines.
    const vector<row_height_t>& rowHeights = visRange.row_heights;
    double offsetY = 0.0;
    for (size_t i = 0; i < rowHeights.size(); ++i)
    {
        double rowHeight = rowHeights[i] / 1440.0 * dpi; // convert twips to pixels.

        cr->move_to(0, offsetY + rowHeight);
        cr->line_to(width, offsetY + rowHeight);
        cr->stroke();

        offsetY += rowHeight;
    }

    // Draw vertical gridlines.
    const vector<col_width_t>& colWidths = visRange.col_widths;;
    double offsetX = 0.0;
    for (size_t i = 0; i < colWidths.size(); ++i)
    {
        double colWidth = colWidths[i] / 1440.0 * dpi; // convert twips to pixels.

        // Draw the separator.
        cr->move_to(offsetX + colWidth, 0);
        cr->line_to(offsetX + colWidth, height);
        cr->stroke();

        offsetX += colWidth;
    }

    draw_text(cr);

    return true;
}

namespace {

struct DrawTextParam
{
    Cairo::RefPtr<Cairo::Context> cr;
    Glib::RefPtr<Pango::Layout> layout;
    double dpi;
    double x;
    double y;
    row_t cur_row;
    col_t cur_col;
    VisibleRange vis_range;

    void next_column()
    {
        x += vis_range.col_widths[cur_col-vis_range.first_col] / 1440.0 * dpi;
        y = 0;
        cur_row = vis_range.first_row;
        ++cur_col;
    }
};

void draw_cell_text(DrawTextParam& param, const char* str, double width, double height)
{
    param.layout->set_text(str);
    int w, h;
    param.layout->get_size(w, h);
    double textW = w, textH = h;
    textW /= Pango::SCALE;
    textH /= Pango::SCALE;

    double textX = param.x, textY = param.y;

    // Bottom align the text by default.
    {
        textY += height - textH;
    }

    param.cr->move_to(textX, textY);
    param.layout->show_in_cairo_context(param.cr);
}

void draw_numeric_block(
    DrawTextParam& param, const ixion::column_store_t::element_block_type& data, size_t skip)
{
    typedef ixion::numeric_element_block block_type;
    block_type::const_iterator it_blk = block_type::begin(data);
    block_type::const_iterator it_blk_end = block_type::end(data);
    std::advance(it_blk, skip);
    for (; it_blk != it_blk_end; ++it_blk)
    {
        double cellW = param.vis_range.col_widths[param.cur_col-param.vis_range.first_col] / 1440.0 * param.dpi;
        double cellH = param.vis_range.row_heights[param.cur_row-param.vis_range.first_row] / 1440.0 * param.dpi;

        ostringstream os;
        os << *it_blk;
        draw_cell_text(param, os.str().c_str(), cellW, cellH);

        param.y += cellH;
        ++param.cur_row;

        if (param.cur_row > param.vis_range.last_row)
            return;
    }
}

void draw_string_block(
    DrawTextParam& param, const ixion::model_context& content, const ixion::column_store_t::element_block_type& data, size_t skip)
{
    typedef ixion::string_element_block block_type;
    block_type::const_iterator it_blk = block_type::begin(data);
    block_type::const_iterator it_blk_end = block_type::end(data);
    std::advance(it_blk, skip);
    for (; it_blk != it_blk_end; ++it_blk)
    {
        double cellW = param.vis_range.col_widths[param.cur_col-param.vis_range.first_col] / 1440.0 * param.dpi;
        double cellH = param.vis_range.row_heights[param.cur_row-param.vis_range.first_row] / 1440.0 * param.dpi;

        const std::string* p = content.get_string(*it_blk);
        if (p)
            draw_cell_text(param, p->c_str(), cellW, cellH);

        param.y += cellH;
        ++param.cur_row;

        if (param.cur_row > param.vis_range.last_row)
            return;
    }
}

void draw_formula_block(
    DrawTextParam& param, const ixion::model_context& content, const ixion::column_store_t::element_block_type& data, size_t skip)
{
    typedef ixion::formula_element_block block_type;
    block_type::const_iterator it_blk = block_type::begin(data);
    block_type::const_iterator it_blk_end = block_type::end(data);
    std::advance(it_blk, skip);
    for (; it_blk != it_blk_end; ++it_blk)
    {
        double cellW = param.vis_range.col_widths[param.cur_col-param.vis_range.first_col] / 1440.0 * param.dpi;
        double cellH = param.vis_range.row_heights[param.cur_row-param.vis_range.first_row] / 1440.0 * param.dpi;

        const ixion::formula_cell* p = *it_blk;
        const ixion::formula_result* res = p->get_result_cache();
        if (res)
        {
            ostringstream os;
            switch (res->get_type())
            {
                case ixion::formula_result::rt_value:
                    os << res->get_value();
                break;
                case ixion::formula_result::rt_string:
                {
                    const std::string* str = content.get_string(res->get_string());
                    if (str)
                        os << *str;
                }
                break;
                case ixion::formula_result::rt_error:
                {
                    ixion::formula_error_t err = res->get_error();
                    os << *ixion::get_formula_error_name(err);
                }
                break;
                default:
                    ;
            }

            draw_cell_text(param, os.str().c_str(), cellW, cellH);
        }

        param.y += cellH;
        ++param.cur_row;

        if (param.cur_row > param.vis_range.last_row)
            return;
    }
}

}

void SheetGrid::draw_text(const Cairo::RefPtr<Cairo::Context>& cr)
{
    const ViewModel& viewModel = m_top_win.get_view_model();
    double dpi = viewModel.get_global_view_properties().get_dpi();

    const DocModel& docModel = m_top_win.get_doc_model();
    const ixion::model_context& content = docModel.get_doc().get_model_context();

    DrawTextParam param;
    param.cr = cr;
    param.dpi = dpi;
    param.x = 0.0;
    param.y = 0.0;
    param.vis_range = m_top_win.get_view_model().get_current_sheet_view().get_visible_range();
    param.cur_row = param.vis_range.first_row;
    param.cur_col = param.vis_range.first_col;
    param.layout = create_pango_layout("");

    Gdk::Cairo::set_source_rgba(cr, Gdk::RGBA("black"));
    Pango::FontDescription font;
    font.set_family("Arial");
    font.set_size(10.0 * Pango::SCALE);
    param.layout->set_font_description(font);

    for (ixion::col_t col = param.vis_range.first_col; col <= param.vis_range.last_col; ++col, param.next_column())
    {
        const ixion::column_store_t* column = content.get_column(0, col);

        if (!column)
            continue;

        // Draw text.

        std::pair<ixion::column_store_t::const_iterator, size_t> ret = column->position(param.vis_range.first_row);
        ixion::column_store_t::const_iterator it = ret.first;
        ixion::column_store_t::const_iterator it_end = column->end();

        if (it == it_end)
            // Column is empty.
            continue;

        size_t offset_block1 = ret.second;

        switch (it->type)
        {
            case ixion::element_type_numeric:
                draw_numeric_block(param, *it->data, offset_block1);
            break;
            case ixion::element_type_string:
                draw_string_block(param, content, *it->data, offset_block1);
            break;
            case ixion::element_type_formula:
                draw_formula_block(param, content, *it->data, offset_block1);
            break;
            default:
            {
                size_t skip = it->size - offset_block1;
                for (size_t i = 0; i < skip; ++i)
                {
                    size_t pos = param.cur_row - param.vis_range.first_row;
                    if (pos >= param.vis_range.row_heights.size())
                        break;
                    param.y += param.vis_range.row_heights[pos] / 1440.0 * param.dpi;
                    ++param.cur_row;
                }
            }
        }

        for (++it ;it != it_end && param.cur_row <= param.vis_range.last_row; ++it)
        {
            switch (it->type)
            {
                case ixion::element_type_numeric:
                    draw_numeric_block(param, *it->data, 0);
                break;
                case ixion::element_type_string:
                    draw_string_block(param, content, *it->data, 0);
                break;
                case ixion::element_type_formula:
                    draw_formula_block(param, content, *it->data, 0);
                break;
                default:
                {
                    size_t skip = it->size;
                    for (size_t i = 0; i < skip; ++i)
                    {
                        size_t pos = param.cur_row - param.vis_range.first_row;
                        if (pos >= param.vis_range.row_heights.size())
                            break;
                        param.y += param.vis_range.row_heights[pos] / 1440.0 * param.dpi;
                        ++param.cur_row;
                    }
                }
            }
        }
    }
}

}
