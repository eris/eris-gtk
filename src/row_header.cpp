/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/row_header.hpp"
#include "eris/view_model.hpp"
#include "eris/view_properties.hpp"
#include "eris/top_window.hpp"

#include <iostream>
#include <sstream>
#include <vector>

#include <orcus/spreadsheet/sheet.hpp>

#include <gdkmm/general.h>

using namespace std;
using namespace orcus::spreadsheet;

namespace eris {

RowHeader::RowHeader(TopWindow& tw) :
    Glib::ObjectBase("row-header"),
    Gtk::Widget(),
    m_top_win(tw),
    m_width(30)
{
    set_has_window(true);
    set_hexpand(false);
    set_vexpand(true);

    //Install a style so that an aspect of this widget may be themed via a CSS
    //style sheet file:
    gtk_widget_class_install_style_property(
        GTK_WIDGET_CLASS(G_OBJECT_GET_CLASS(gobj())),
        g_param_spec_int("example_scale",
                         "Scale of Example Drawing",
                         "The scale to use when drawing. This is just a silly example.",
                         G_MININT, G_MAXINT, 500, G_PARAM_READABLE));

    m_style_provider = Gtk::CssProvider::create();
    Glib::RefPtr<Gtk::StyleContext> refStyleContext = get_style_context();
    refStyleContext->add_provider(m_style_provider,
                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

RowHeader::~RowHeader()
{
}

void RowHeader::recalc_width()
{
    ViewModel& vm = m_top_win.get_view_model();
    const GlobalViewProperties& gprop = vm.get_global_view_properties();
    SheetViewProperties& sview = vm.get_current_sheet_view();

    // Re-calculate necessary width.
    row_t last_row = sview.get_visible_range().last_row;
    ostringstream os;
    os << last_row;
    Glib::RefPtr<Pango::Layout> layout = create_pango_layout(os.str().c_str());
    Pango::FontDescription font = gprop.get_header_font();
    layout->set_font_description(font);

    int w, h;
    layout->get_size(w, h);
    int preferred_width = w / Pango::SCALE * 1.5;
    if (preferred_width < 30)
        preferred_width = 30;

    m_width = preferred_width;
}

Gtk::SizeRequestMode RowHeader::get_request_mode_vfunc() const
{
    return Gtk::Widget::get_request_mode_vfunc();
}

void RowHeader::get_preferred_width_vfunc(int& minimum_width, int& natural_width) const
{
    minimum_width = m_width;
    natural_width = m_width;
}

void RowHeader::get_preferred_height_for_width_vfunc(
    int /* width */, int& minimum_height, int& natural_height) const
{
    minimum_height = 500;
    natural_height = 700;
}

void RowHeader::get_preferred_height_vfunc(int& minimum_height, int& natural_height) const
{
    minimum_height = 500;
    natural_height = 700;
}

void RowHeader::get_preferred_width_for_height_vfunc(
    int /* height */, int& minimum_width, int& natural_width) const
{
    minimum_width = m_width;
    natural_width = m_width;
}

void RowHeader::on_size_allocate(Gtk::Allocation& allocation)
{
    // Use the offered allocation for this container:
    set_allocation(allocation);

    if (m_gdk_win)
    {
        m_gdk_win->move_resize(
            allocation.get_x(), allocation.get_y(),
            allocation.get_width(), allocation.get_height());
    }

    // Recalculate visible row range.
    ViewModel& vm = m_top_win.get_view_model();
    SheetViewProperties& sview = vm.get_current_sheet_view();
    sview.update_visible_row_range(allocation.get_height());

    recalc_width();
}

void RowHeader::on_map()
{
    //Call base class:
    Gtk::Widget::on_map();
}

void RowHeader::on_unmap()
{
    //Call base class:
    Gtk::Widget::on_unmap();
}

void RowHeader::on_realize()
{
    //Do not call base class Gtk::Widget::on_realize().
    //It's intended only for widgets that set_has_window(false).

    set_realized();

    //Get the themed style from the CSS file:
    get_style_property("example_scale", m_scale);

    if (!m_gdk_win)
    {
        //Create the GdkWindow:

        GdkWindowAttr attributes;
        memset(&attributes, 0, sizeof(attributes));

        Gtk::Allocation allocation = get_allocation();

        //Set initial position and size of the Gdk::Window:
        attributes.x = allocation.get_x();
        attributes.y = allocation.get_y();
        attributes.width = allocation.get_width();
        attributes.height = allocation.get_height();

        attributes.event_mask = get_events() | Gdk::EXPOSURE_MASK;
        attributes.window_type = GDK_WINDOW_CHILD;
        attributes.wclass = GDK_INPUT_OUTPUT;

        m_gdk_win = Gdk::Window::create(get_parent_window(), &attributes,
                                             GDK_WA_X | GDK_WA_Y);
        set_window(m_gdk_win);

        //set colors
        override_background_color(Gdk::RGBA("rgb(148,189,94)"));
        override_color(Gdk::RGBA("white"));

        //make the widget receive expose events
        m_gdk_win->set_user_data(gobj());
    }
}

void RowHeader::on_unrealize()
{
    m_gdk_win.reset();

    //Call base class:
    Gtk::Widget::on_unrealize();
}

bool RowHeader::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    double width = get_allocated_width();

    // paint the background
    Gdk::Cairo::set_source_rgba(cr, get_style_context()->get_background_color());
    cr->paint();

    // draw the foreground
    Gdk::Cairo::set_source_rgba(cr, get_style_context()->get_color());
    cr->set_line_width(1.0);

    const ViewModel& viewModel = m_top_win.get_view_model();
    double dpi = viewModel.get_global_view_properties().get_dpi();
    Pango::FontDescription font = viewModel.get_global_view_properties().get_header_font();

    const VisibleRange& visRange = viewModel.get_current_sheet_view().get_visible_range();
    const vector<row_height_t>& rowHeights = visRange.row_heights;

    double offsetY = 0.0;
    for (size_t i = 0; i < rowHeights.size(); ++i)
    {
        double rowHeight = rowHeights[i] / 1440.0 * dpi; // convert twips to pixels.

        // Draw the separator.
        cr->move_to(0, offsetY + rowHeight);
        cr->line_to(width, offsetY + rowHeight);
        cr->stroke();

        // Draw the text label center-aligned.
        ostringstream os;
        os << (i + visRange.first_row + 1); // dispalyed row is 1-based.
        Glib::RefPtr<Pango::Layout> layout = create_pango_layout(os.str().c_str());
        layout->set_font_description(font);
        int layoutWidth, layoutHeight;
        layout->get_size(layoutWidth, layoutHeight);

        double x = (width - static_cast<double>(layoutWidth)/Pango::SCALE) / 2.0;
        double y = (rowHeight - static_cast<double>(layoutHeight)/Pango::SCALE) / 2.0;

        cr->move_to(x, y + offsetY);
        layout->show_in_cairo_context(cr);

        offsetY += rowHeight;
    }

    return true;
}

}


