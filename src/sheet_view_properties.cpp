/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/sheet_view_properties.hpp"
#include "eris/doc_model.hpp"
#include "eris/view_model.hpp"

#include <orcus/spreadsheet/sheet.hpp>

#include <cmath>
#include <iostream>

using namespace std;
using namespace orcus::spreadsheet;

namespace eris {

SheetViewProperties::SheetViewProperties(ViewModel& viewModel, sheet_t sheet_pos) :
    m_view_model(viewModel), m_view_pos(sheet_pos,0,0),
    m_visible_width(0.0),
    m_visible_height(0.0),
    m_visible_row_range_valid(false),
    m_visible_col_range_valid(false)
{
}

SheetViewProperties::~SheetViewProperties()
{
}

void SheetViewProperties::invalidate_visible_range()
{
    m_visible_col_range_valid = false;
    m_visible_row_range_valid = false;
}

void SheetViewProperties::set_view_position(ixion::row_t row, ixion::col_t col)
{
    m_view_pos.row = row;
    m_view_pos.column = col;
}

const ixion::abs_address_t& SheetViewProperties::get_view_position() const
{
    return m_view_pos;
}

void SheetViewProperties::update_visible_row_range(int height)
{
    if (m_visible_row_range_valid)
        return;

    const sheet* content = m_view_model.get_doc_model().get_doc().get_sheet(m_view_pos.sheet);

    // Convert screen size to twips.
    double dpi = m_view_model.get_global_view_properties().get_dpi();
    double scrH = height / dpi * 1440.0;
    m_visible_height = scrH;

    // First cell position won't change in this call.
    row_height_t default_row_height = m_view_model.get_doc_model().get_default_row_height();

    // Start from the view position (top-left cell of visible region).
    m_visible_range.first_row = m_view_pos.row;
    std::vector<row_height_t> rowHeights;

    if (content)
    {
        double totalHeight = 0;
        row_t rowEnd = -1;
        double rowh = 0;
        for (row_t row = m_view_pos.row; row < content->row_size(); ++row)
        {
            if (row >= rowEnd)
            {
                // Update the current row height.
                rowh = content->get_row_height(row, NULL, &rowEnd);
                if (rowh == orcus::spreadsheet::default_row_height)
                    rowh = default_row_height;
            }

            rowHeights.push_back(rowh);
            if (totalHeight + rowh > scrH)
            {
                m_visible_range.last_row = row;
                break;
            }
            totalHeight += rowh;
        }
    }
    else
    {
        // It's an empty content.
        int rows = ceil(scrH / default_row_height);
        rowHeights.resize(rows, default_row_height);
        m_visible_range.last_row = m_visible_range.first_row + rows - 1;
    }

    m_visible_range.row_heights.swap(rowHeights);
    m_visible_row_range_valid = true;
}

void SheetViewProperties::update_visible_col_range(int width)
{
    if (m_visible_col_range_valid)
        return;

    const sheet* content = m_view_model.get_doc_model().get_doc().get_sheet(m_view_pos.sheet);

    // Convert screen size to twips.
    double dpi = m_view_model.get_global_view_properties().get_dpi();
    double scrW = width / dpi * 1440.0;
    m_visible_width = scrW;

    // First cell position won't change in this call.
    col_width_t default_col_width = m_view_model.get_doc_model().get_default_col_width();

    // Start from the view position (top-left cell of visible region).
    m_visible_range.first_col = m_view_pos.column;
    std::vector<col_width_t> colWidths;

    if (content)
    {
        double totalWidth = 0;
        col_t colEnd = -1;
        double colw = 0;
        for (col_t col = m_view_pos.column; col < content->col_size(); ++col)
        {
            if (col >= colEnd)
            {
                // Update the current column width.
                colw = content->get_col_width(col, NULL, &colEnd);
                if (colw == orcus::spreadsheet::default_column_width)
                    colw = default_col_width;
            }

            colWidths.push_back(colw);
            if (totalWidth + colw > scrW)
            {
                m_visible_range.last_col = col;
                break;
            }

            totalWidth += colw;
        }
    }
    else
    {
        // It's an empty content.
        int cols = ceil(scrW / default_col_width);
        m_visible_range.last_col = m_visible_range.first_col + cols - 1;
        colWidths.resize(cols, default_col_width);
    }

    m_visible_range.col_widths.swap(colWidths);
    m_visible_col_range_valid = true;
}

const VisibleRange& SheetViewProperties::get_visible_range() const
{
    return m_visible_range;
}

void SheetViewProperties::move_right()
{
    const DocModel& doc = m_view_model.get_doc_model();
    col_t max_col = doc.get_doc().get_sheet(m_view_pos.sheet)->col_size()-1;
    if (m_view_pos.column < max_col)
        ++m_view_pos.column;

    refresh_visible_range();
}

void SheetViewProperties::move_left()
{
    if (m_view_pos.column > 0)
        --m_view_pos.column;

    refresh_visible_range();
}

void SheetViewProperties::move_up()
{
    if (m_view_pos.row > 0)
        --m_view_pos.row;

    refresh_visible_range();
}

void SheetViewProperties::move_down()
{
    const DocModel& doc = m_view_model.get_doc_model();
    row_t max_row = doc.get_doc().get_sheet(m_view_pos.sheet)->row_size()-1;
    if (m_view_pos.row < max_row)
        ++m_view_pos.row;

    refresh_visible_range();
}

void SheetViewProperties::page_up()
{
    // Move up the visible area such that the first row in the current view
    // will become the last row in the new view.

    const sheet* content = m_view_model.get_doc_model().get_doc().get_sheet(m_view_pos.sheet);
    if (!content)
        // This should never happen.
        return;

    row_height_t default_row_height = m_view_model.get_doc_model().get_default_row_height();

    double total_height = 0.0;
    row_t row_first = m_visible_range.first_row + 1;
    row_height_t height = 0;
    row_t row = m_visible_range.first_row;
    for (; row >= 0; --row)
    {
        if (row < row_first)
        {
            height = content->get_row_height(row, &row_first, NULL);
            if (height == orcus::spreadsheet::default_row_height)
                height = default_row_height;
        }

        total_height += height;
        if (total_height > m_visible_height)
            break;
    }

    m_view_pos.row = row;
    if (m_view_pos.row < 0)
        m_view_pos.row = 0;

    refresh_visible_range();
}

void SheetViewProperties::page_down()
{
    const DocModel& doc = m_view_model.get_doc_model();
    row_t max_row = doc.get_doc().get_sheet(m_view_pos.sheet)->row_size()-1;
    m_view_pos.row = m_visible_range.last_row;
    if (m_view_pos.row > max_row)
        m_view_pos.row = max_row;

    refresh_visible_range();
}

void SheetViewProperties::move_home()
{
    m_view_pos.row = 0;
    m_view_pos.column = 0;
    refresh_visible_range();
}

void SheetViewProperties::move_row_home()
{
    m_view_pos.column = 0;
    refresh_visible_range();
}

void SheetViewProperties::refresh_visible_range()
{
    invalidate_visible_range();
    update_visible_col_range(m_visible_width);
    update_visible_row_range(m_visible_height);
}

}
