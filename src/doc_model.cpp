/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/doc_model.hpp"

#include <orcus/spreadsheet/document.hpp>
#include <orcus/pstring.hpp>

using namespace std;

namespace eris {

DocModel::DocModel() :
    m_default_col_width(1500),
    m_default_row_height(400),
    m_default_col_size(1024),
    m_default_row_size(1048576)
{
    m_document.append_sheet("Sheet1", m_default_row_size, m_default_col_size);
}

DocModel::~DocModel() {}

col_width_t DocModel::get_default_col_width() const
{
    return m_default_col_width;
}

row_height_t DocModel::get_default_row_height() const
{
    return m_default_row_height;
}

col_t DocModel::get_default_col_size() const
{
    return m_default_col_size;
}

row_t DocModel::get_default_row_size() const
{
    return m_default_row_size;
}

void DocModel::swap_document(orcus::spreadsheet::document& doc)
{
    m_document.swap(doc);
}

const orcus::spreadsheet::document& DocModel::get_doc() const
{
    return m_document;
}

}
