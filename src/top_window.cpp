/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/top_window.hpp"
#include "eris/file_loader.hpp"

#include <gtkmm/main.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/stock.h>
#include <glibmm/main.h>

#include <iostream>
#include <string>

using namespace std;

namespace eris {

namespace {

string detect_format(const string& filepath)
{
    size_t pos = filepath.find_last_of(".");
    if (pos == string::npos)
        return string();

    string::const_iterator it = filepath.begin();
    std::advance(it, pos);
    ++it;
    string ext(it, filepath.end());
    return ext;
}

}

TopWindow::TopWindow() :
    m_view_model(m_doc_model),
    m_vbar_adj(Gtk::Adjustment::create(0, 0, 10, 1, 10, 7)),
    m_hbar_adj(Gtk::Adjustment::create(0, 0, 10, 1, 10, 7)),
    m_vbar(m_vbar_adj, Gtk::ORIENTATION_VERTICAL),
    m_hbar(m_hbar_adj, Gtk::ORIENTATION_HORIZONTAL),
    m_status_text("Ready"),
    m_sheet_grid(*this),
    m_col_header(*this),
    m_row_header(*this)
{
    set_title("Eris GTK");
    set_border_width(0);

    add(m_vbox_top);

    m_vbox_top.pack_start(m_grid_scrollbars, true, true);
    m_vbox_top.pack_end(m_status_bar, false, false);

    m_status_bar.set_border_width(3);
    m_status_bar.set_spacing(5);
    m_status_bar.pack_start(m_status_text, false, false);
    m_status_bar.pack_start(m_load_progress);

    m_grid_scrollbars.attach(m_grid_content, 0, 0, 1, 1);
    m_grid_scrollbars.attach(m_vbar, 1, 0, 1, 1);
    m_grid_scrollbars.attach(m_hbar, 0, 1, 1, 1);

    m_grid_content.attach(m_col_header, 1, 0, 1, 1);
    m_grid_content.attach(m_row_header, 0, 1, 1, 1);
    m_grid_content.attach(m_sheet_grid, 1, 1, 1, 1);

    show_all_children();
    m_load_progress.hide();

    m_vbar_adj->signal_value_changed().connect(
        sigc::mem_fun(*this, &TopWindow::on_vbar_value_changed));

    m_hbar_adj->signal_value_changed().connect(
        sigc::mem_fun(*this, &TopWindow::on_hbar_value_changed));

    add_events(Gdk::KEY_PRESS_MASK);

    FileLoader::init();
}


TopWindow::~TopWindow()
{
}

ViewModel& TopWindow::get_view_model()
{
    return m_view_model;
}

const ViewModel& TopWindow::get_view_model() const
{
    return m_view_model;
}

DocModel& TopWindow::get_doc_model()
{
    return m_doc_model;
}

const DocModel& TopWindow::get_doc_model() const
{
    return m_doc_model;
}

void TopWindow::on_vbar_value_changed()
{
    cout << "vbar value: " << m_vbar_adj->get_value() << endl;
}

void TopWindow::on_hbar_value_changed()
{
    cout << "hbar value: " << m_hbar_adj->get_value() << endl;
}

void TopWindow::on_size_allocate(Gtk::Allocation& allocation)
{
    get_view_model().get_current_sheet_view().invalidate_visible_range();
    Gtk::Window::on_size_allocate(allocation);
}

bool TopWindow::on_key_press_event(GdkEventKey* event)
{
    if (FileLoader::is_loading())
        return Gtk::Window::on_key_press_event(event);

    bool ctrl = (event->state & GDK_CONTROL_MASK);
    bool shift = (event->state & GDK_SHIFT_MASK);
    if (ctrl && !shift)
    {
        if (event->keyval == GDK_KEY_o)
        {
            // Ctrl-O pressed.
            launch_file_chooser_dialog();
            return true;
        }
    }

    if (!ctrl && !shift)
    {
        switch (event->keyval)
        {
            case GDK_KEY_Up:
                m_view_model.get_current_sheet_view().move_up();
                view_changed();
                return true;
            case GDK_KEY_Down:
                m_view_model.get_current_sheet_view().move_down();
                view_changed();
                return true;
            case GDK_KEY_Left:
                m_view_model.get_current_sheet_view().move_left();
                view_changed();
                return true;
            case GDK_KEY_Right:
                m_view_model.get_current_sheet_view().move_right();
                view_changed();
                return true;
            case GDK_KEY_Page_Down:
                m_view_model.get_current_sheet_view().page_down();
                view_changed();
                return true;
            case GDK_KEY_Page_Up:
                m_view_model.get_current_sheet_view().page_up();
                view_changed();
                return true;
            case GDK_KEY_Home:
                m_view_model.get_current_sheet_view().move_row_home();
                view_changed();
                return true;
            default:
                ;
        }
    }
    else if (ctrl && !shift)
    {
        switch (event->keyval)
        {
            case GDK_KEY_Home:
                m_view_model.get_current_sheet_view().move_home();
                view_changed();
                return true;
            default:
                ;
        }
    }

    return Gtk::Window::on_key_press_event(event);
}

void TopWindow::launch_file_chooser_dialog()
{
    Gtk::FileChooserDialog dialog("Choose a file to open.", Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button("Open", Gtk::RESPONSE_OK);

    if (dialog.run() != Gtk::RESPONSE_OK)
        return;

    Glib::signal_timeout().connect(
        sigc::mem_fun(*this, &TopWindow::on_load_timeout), 100);

    string filepath = dialog.get_filename();
    FileLoader::load(
        filepath, detect_format(filepath),
        m_doc_model.get_default_row_size(), m_doc_model.get_default_col_size());

    m_status_text.set_text("Loading file...");
    m_load_progress.show();
}

void TopWindow::view_changed()
{
    // Re-calculate size of column row headers and the sheet area to calculate
    // the visible range.
    m_row_header.recalc_width(); // row label width may have changed.

    m_col_header.queue_resize();
    m_row_header.queue_resize();
    m_sheet_grid.queue_resize();
}

bool TopWindow::on_load_timeout()
{
    bool still_loading = FileLoader::is_loading();

    if (still_loading)
    {
        cout << "loading..." << endl;
        m_load_progress.pulse();
    }
    else if (FileLoader::success())
    {
        // Finished loading the file. Refresh the view.
        orcus::spreadsheet::document doc;
        FileLoader::swap_document(doc); // get the doc from the loader.
        m_doc_model.swap_document(doc); // set the new doc to the model.

        m_view_model.init_sheet_views(m_doc_model.get_doc().sheet_size());
        view_changed();
        m_load_progress.hide();
        m_status_text.set_text("Ready");
    }
    else
    {
        // Load failed.
        m_load_progress.hide();
        m_status_text.set_text("Ready");
    }

    return still_loading;
}

}
