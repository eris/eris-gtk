/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/view_model.hpp"
#include "eris/doc_model.hpp"

#include <cmath>
#include <iostream>

#include <orcus/spreadsheet/sheet.hpp>
#include <ixion/formula_name_resolver.hpp>
#include <ixion/model_context.hpp>

using namespace std;
using namespace orcus::spreadsheet;

namespace eris {

ViewModel::ViewModel(const DocModel& doc) :
    m_doc_model(doc),
    m_cur_sheet(0)
{
    m_sheet_views.push_back(new SheetViewProperties(*this, 0));
}

const DocModel& ViewModel::get_doc_model() const
{
    return m_doc_model;
}

void ViewModel::init_sheet_views(size_t sheet_count)
{
    m_sheet_views.clear();
    for (size_t i = 0; i < sheet_count; ++i)
        m_sheet_views.push_back(new SheetViewProperties(*this, i));
}

orcus::spreadsheet::sheet_t ViewModel::get_current_sheet_index() const
{
    return m_cur_sheet;
}

SheetViewProperties& ViewModel::get_current_sheet_view()
{
    return m_sheet_views[m_cur_sheet];
}

const SheetViewProperties& ViewModel::get_current_sheet_view() const
{
    return m_sheet_views[m_cur_sheet];
}

const GlobalViewProperties& ViewModel::get_global_view_properties() const
{
    return m_global_view;
}

string ViewModel::get_column_label(col_t col) const
{
    const ixion::formula_name_resolver& resolver =
        m_doc_model.get_doc().get_model_context().get_name_resolver();

    return resolver.get_column_name(col);
}

}
