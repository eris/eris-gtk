/*************************************************************************
 *
 * Copyright (c) 2013 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "eris/file_loader.hpp"

#include <orcus/spreadsheet/document.hpp>
#include <orcus/spreadsheet/factory.hpp>
#include <orcus/orcus_ods.hpp>
#include <orcus/orcus_xlsx.hpp>
#include <orcus/orcus_csv.hpp>
#include <orcus/orcus_gnumeric.hpp>

#include <iostream>

#include <atomic>

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

using namespace std;

namespace eris {

namespace {

/**
 * Parameters used during file load. Synchronized with a mutex shared for
 * all parameters.
 */
struct
{
    orcus::spreadsheet::document doc;
    std::string filepath;
    std::string format;
    row_t row_size;
    col_t col_size;
    bool success;

    boost::mutex mtx;

} load_file_param;

boost::thread thread_load_file;
atomic<bool> thread_load_file_running;

bool load(orcus::spreadsheet::document& doc, const string& filepath, const string& format_type, row_t row_size, col_t col_size)
{
    cout << "filepath: " << filepath << " (" << format_type << ")" << endl;

    try
    {
        orcus::spreadsheet::document newdoc;
        orcus::spreadsheet::import_factory fact(&newdoc, row_size, col_size);

        if (format_type == "ods")
        {
            orcus::orcus_ods loader(&fact);
            loader.read_file(filepath.c_str());
        }
        else if (format_type == "xlsx")
        {
            orcus::orcus_xlsx loader(&fact);
            loader.read_file(filepath.c_str());
        }
        else if (format_type == "csv")
        {
            orcus::orcus_csv loader(&fact);
            loader.read_file(filepath.c_str());
        }
        else if (format_type == "gnumeric")
        {
            orcus::orcus_gnumeric loader(&fact);
            loader.read_file(filepath.c_str());
        }
        else
            return false;

        if (!newdoc.sheet_size())
            return false;

        doc.swap(newdoc);
        return true;
    }
    catch (std::exception& e)
    {
        cerr << e.what() << endl;
    }
    return false;
}

void load_file()
{
    string filepath, format;
    row_t row_size;
    col_t col_size;
    {
        boost::mutex::scoped_lock lock(load_file_param.mtx);
        filepath = load_file_param.filepath;
        format = load_file_param.format;
        row_size = load_file_param.row_size;
        col_size = load_file_param.col_size;
    }

    orcus::spreadsheet::document doc;
    bool success = load(doc, filepath, format, row_size, col_size);

    if (success)
    {
        boost::mutex::scoped_lock lock(load_file_param.mtx);
        load_file_param.doc.swap(doc);
        load_file_param.success = success;
    }

    thread_load_file_running = false;
}

}

void FileLoader::init()
{
    thread_load_file_running = false;
}

void FileLoader::load(const std::string& filepath, const std::string& format, row_t row_size, col_t col_size)
{
    thread_load_file_running = true;

    {
        boost::mutex::scoped_lock lock(load_file_param.mtx);
        load_file_param.filepath = filepath;
        load_file_param.format = format;
        load_file_param.row_size = row_size;
        load_file_param.col_size = col_size;
        load_file_param.success = false;
    }

    boost::thread t(load_file);
    thread_load_file.swap(t);
}

bool FileLoader::is_loading()
{
    return thread_load_file_running;
}

bool FileLoader::success()
{
    boost::mutex::scoped_lock lock(load_file_param.mtx);
    return load_file_param.success;
}

void FileLoader::swap_document(orcus::spreadsheet::document& doc)
{
    boost::mutex::scoped_lock lock(load_file_param.mtx);
    load_file_param.doc.swap(doc);
}

}
